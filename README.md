KiCad 3d Model Generator Scripts
===
This repository contains a number of scripts to generate STEP AP214 3D models and VRML 3D models for use with the KiCad EDA system

The parametric scripts are derived from CadQuery scripts for generating
QFP, DIP and pinHeaders models in X3D format.  
author **hyOzd**  
author site: <https://bitbucket.org/hyOzd/freecad-macros/>  
These were greatly extended by **easyw** in the repository <https://github.com/easyw/kicad-3d-models-in-freecad>

Requirements to run these scripts:  
[CadQuery 2](https://github.com/CadQuery/cadquery)  
[Python 3](https://www.python.org/)  
[OpenCascade 7](https://dev.opencascade.org/doc/overview/html/index.html)

Some modules still to be converted require  
[CadQuery
module](https://github.com/jmwright/cadquery-freecad-module/archive/master.zip/)
version 0.30  
[FreeCAD](http://freecadweb.org/) version 0.15 or later

Credits
=======

author **hyOzd**  
author site: <https://bitbucket.org/hyOzd/freecad-macros/>  
FreeCAD & cadquery tools:  
libraries to export generated models in STEP & VRML format  
- cad tools functions  
Copyright (c) 2015 **Maurice** <https://launchpad.net/~easyw>  

Copyright
=========

This document *README* and all the materials and files at the repository
are  
Copyright © 2015 by Maurice.  
Copyright © 2020 by the KiCad EDA project  

License
=======
Please see the [LICENSE file](LICENSE) for licensing details